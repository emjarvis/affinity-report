with constituents as (	
  select distinct p.idnumber	
  from prospect p	
  join payment pay on (p.idnumber=pay.idnumber and extract(year from case when pay.paydate is null then pay.timestamp else pay.paydate end) >= extract(year from sysdate)-5)	
  join gift g on (pay.ltransnum=g.transnum)	
  where g.giftamount > 0	
  and p.solicit = 'Y'	
  and p.recordtype = 'I'	
  and $X{IN, p.solicitorcode, Account_Manager}	
  minus	
  select idnumber	
  from additional_demographics	
  where deceased = 'Y'	
  or dod is not null	
  minus	
  select idnumber	
  from interest	
  where intcode in ('NOCO','NC')	
  and (end_date > sysdate or 
       end_date is null)	
  ),	
email_constituents as (	
  select idnumber	
  from constituents	
  intersect	
  (select idnumber	
   from registration	
   where case when t_date is null then timestamp else t_date end >= sysdate-730	
   and usercode5 = '4CN'	
   union	
   select idnumber	
   from payment	
   where case when paydate is null then timestamp else paydate end >=sysdate-730	
   )	
  intersect	
  select idnumber	
  from prospect	
  where emailflag = 'Y'	
  ),	
phone_constituents as (	
  select idnumber	
  from constituents	
  intersect	
  select idnumber	
  from prospect	
  where phoneflag = 'Y'	
  ),	
region as (	
  select sub.unit_id, sub.unit_name as chapter, par.unit_name as region	
  from w_prsunt_d wpu, w_unt_d sub, w_unt_d par	
  where wpu.sub_unit_id = sub.unit_id	
  and wpu.parent_unit_id = par.unit_id	
  and (wpu.parent_unit_levelno = 6	
       or (sub_unit_id = 2107862	
           and wpu.parent_unit_levelno = 8	
           )	
       )	
  ),	
t1d as (	
  select idnumber,	
  case when idnumber in (select idnumber from interest where intcode in ('DCME')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCCH','DCDA','DCSO')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCBO','DCHU','DCWI')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCGC','DCGD','DCGS')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCFA','DCMO')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCBR','DCSI')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCGF','DCGM')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCNE','DCNI')) then 10 else 0 end +	
    case when idnumber in (select idnumber from interest where intcode in ('DCAU','DCUN','DCCO','DCRE','DCGI','DCFR','DCOT')) then 10 else 0 end	
    as connection	
  from constituents	
  where idnumber in (select idnumber	
                     from interest	
                     where intcode in ('DCME',	
                                       'DCCH','DCDA','DCSO',	
                                       'DCBO','DCHU','DCWI',	
                                       'DCGC','DCGD','DCGS',	
                                       'DCFA','DCMO',	
                                       'DCBR','DCSI',	
                                       'DCGF','DCGM',	
                                       'DCNE','DCNI',	
                                       'DCAU','DCUN','DCCO','DCRE','DCGI','DCFR','DCOT'	
                                       )	
                     )	
  ),	
registrations as (	
  select r.idnumber, count(r.transnum)*10 as registrations	
  from constituents con	
  join registration r on (con.idnumber=r.idnumber)	
  join events ev on (r.event=ev.code)	
  where (r.usercode5 = '4CN' or r.usercode5 is null)	
  and case when ev.event_start is null then (case when r.t_date is null then r.timestamp else r.t_date end) else ev.event_start end >= sysdate-366	
  and case when ev.event_start is null then (case when r.t_date is null then r.timestamp else r.t_date end) else ev.event_start end < sysdate	
  group by r.idnumber	
  ),	
consecutive_giving as (	
  select idnumber,	
  case when idnumber in (select idnumber from payment where extract(year from paydate) = extract(year from sysdate)-5) then 1 else 0 end +	
  case when idnumber in (select idnumber from payment where extract(year from paydate) = extract(year from sysdate)-4) then 1 else 0 end +	
  case when idnumber in (select idnumber from payment where extract(year from paydate) = extract(year from sysdate)-3) then 1 else 0 end +	
  case when idnumber in (select idnumber from payment where extract(year from paydate) = extract(year from sysdate)-2) then 1 else 0 end +	
  case when idnumber in (select idnumber from payment where extract(year from paydate) = extract(year from sysdate)-1) then 1 else 0 end	
  as consecutive_giving	
  from constituents	
  ),	
gift_count as (	
  select con.idnumber, count(g.ltransnum) as gift_count	
  from constituents con	
  join (select pay.ltransnum, pay.idnumber, sum(pay.payamount)	
        from constituents con	
        join payment pay on (con.idnumber=pay.idnumber)	
        where extract(year from case when pay.paydate is null then pay.timestamp else pay.paydate end) >= extract(year from sysdate)-6	
        and extract(year from case when pay.paydate is null then pay.timestamp else pay.paydate end) < extract(year from sysdate)	
        group by pay.ltransnum, pay.idnumber	
        having sum(pay.payamount) > 0	
        ) g on (con.idnumber=g.idnumber)	
  group by con.idnumber	
  ),	
years_giving as (	
  select idnumber, count(year) as years_giving	
  from (select distinct con.idnumber, extract(year from case when pay.paydate is null then pay.timestamp else pay.paydate end) as year	
        from constituents con	
        join payment pay on (con.idnumber=pay.idnumber)	
        join gift g on (pay.ltransnum=g.transnum)	
        where g.giftamount > 0	
        )	
  group by idnumber	
  ),	
last_action_completed as (	
  select idnumber, actiondate, actiontype	
  from (	
    select c.idnumber, c.actiondate, ac.description as actiontype,	
    row_number() over (partition by c.idnumber	
                       order by c.actiondate desc	
                      ) as rank	
    from contact c	
    join actions ac on (c.actioncode=ac.code)	
    where c.actiondate is not null	
  )	
  where rank = 1	
  ),	
last_action as (	
  select idnumber, action_date, actiontype	
  from (	
    select c.idnumber, case when c.c_date is null then c.timestamp else c.c_date end as action_date, ac.description as actiontype,	
    row_number() over (partition by c.idnumber	
                       order by case when c.c_date is null then c.timestamp else c.c_date end desc nulls last	
                      ) as rank	
    from contact c	
    join actions ac on (c.actioncode=ac.code)	
  )	
  where rank = 1	
  )	
select con.idnumber,	
p.first,	
p.last,	
t1d.connection,	
case when w2.total_amount >=10000 then 10 else 0 end as lifetime_giving,	
r.registrations,	
case when cg.consecutive_giving >=3 then 10 else 0 end as consecutive_giving,	
case p.usercode9	
  when 'MGC' then 5	
  when 'MGP' then 10	
  else 0 end as major_donor,
gc.gift_count,	
yg.years_giving,	
case when t1d.connection is not null then t1d.connection else 0 end +	
  case when w2.total_amount >=10000 then 10 else 0 end +	
  case when r.registrations is not null then r.registrations else 0 end +	
  case when cg.consecutive_giving >=3 then 10 else 0 end +	
  case p.usercode9 when 'MGC' then 5 when 'MGP' then 10 else 0 end +	
  case when gc.gift_count is not null then gc.gift_count else 0 end +	
  case when yg.years_giving is not null then yg.years_giving else 0 end 
  as total,	
sol.solicitor,	
rp.region,	
rp.chapter,	
e.email,	
case when ph.areacode is not null then '(' || ph.areacode || ') ' || ph.phone	
else ph.phone end ||	
    case when ph.extension is not null then ' ex. ' || ph.extension	
    else null end as phone,	
w2.total_amount as lifetime_hardcredits,
lac.actiondate as completed_action,	
lac.actiontype as completed_type,	
la.action_date as last_action,	
la.actiontype as last_type,	
case when p.recordtype = 'D' then p.recordtype||'ual'	
  when p.recordtype = 'O' then p.recordtype||'rganization'	
  else 'Individual'	
  end as recordtype_desc,
case when con.idnumber in (
  select idnumber
  from tracking
  where status <> '8C'
  ) then 'Yes' else 'No' end as open_opportunity
from constituents con	
join prospect p on (con.idnumber=p.idnumber)	
left join email e on (p.idnumber=e.idnumber	
                      and e.preference = 'Y'	
                      and (e.enddate > sysdate or e.enddate is null)	
                      and e.deliverable = 'Y'	
                      and lower(e.email) not like '%offline%'	
                      and lower(e.email) not like '%noemail%'	
                      and lower(e.email) not like '%donordrive%'	
                      and lower(e.email) not like '%jdrf.raisin%'	
                      and lower(e.email) not like '%jdrfraisin%'	
                      and lower(e.email) not like '%r_servation%'	
                      and lower(e.email) not like '%reserve%'	
                      and e.idnumber in (select * from email_constituents)	
                      )	
left join phone ph on (p.idnumber=ph.idnumber	
                       and ph.preference = 'Y'	
                       and (ph.enddate > sysdate or ph.enddate is null)	
                       and ph.idnumber in (select * from phone_constituents)	
                      )	
left join region rp on (p.unitid=rp.unit_id)	
left join solicitors sol on (p.solicitorcode=sol.solicitorcode)	
left join t1d on (con.idnumber=t1d.idnumber)	
left join w_prs_d w1 on (con.idnumber=w1.idnumber)	
left join w_prs_gvg_a w2 on (w1.key=w2.prospect_key)	
left join registrations r on (con.idnumber=r.idnumber)	
left join consecutive_giving cg on (con.idnumber=cg.idnumber)	
left join gift_count gc on (con.idnumber=gc.idnumber)	
left join years_giving yg on (con.idnumber=yg.idnumber)	
left join last_action_completed lac on (con.idnumber=lac.idnumber)	
left join last_action la on (con.idnumber=la.idnumber)	
left join additional_demographics ad on (con.idnumber=ad.idnumber)	
left join w_prs_d w1 on (con.idnumber=w1.idnumber)	
left join w_prs_gvg_a w2 on (w1.key=w2.prospect_key)	
where $X{IN, rp.region, Region}
$P!{Minimum_SQL} 
$P!{Maximum_SQL} 
order by case when t1d.connection is not null then t1d.connection else 0 end +	
  case when w2.total_amount >=10000 then 10 else 0 end +	
  case when r.registrations is not null then r.registrations else 0 end +	
  case when cg.consecutive_giving >=3 then 10 else 0 end +	
  case p.usercode9 when 'MGC' then 5 when 'MGP' then 10 else 0 end +	
  case when gc.gift_count is not null then gc.gift_count else 0 end +	
  case when yg.years_giving is not null then yg.years_giving else 0 end	
  desc	